<?php declare(strict_types=1);

namespace Finsterforst\Cygnet\Application\Models;

use Finsterforst\Cygnet\Application\Exceptions\User\UserNotFoundException;
use Finsterforst\Cygnet\Application\Exceptions\User\UserWrongPassword;
use Finsterforst\Cygnet\Application\Exceptions\UserWasNotLoadedException;
use Finsterforst\Cygnet\Application\Kernel\Database;
use Finsterforst\Cygnet\Application\Kernel\Router;
use Finsterforst\Cygnet\Application\Kernel\Session;

class User extends BaseModel
{
    private $id;
    
    private $email;
    
    private $password;
    
    private $username;
    
    /**
     * 
     * @param int $id
     * @throws UserWasNotLoadedException
     */
    public function load(int $id) : void {
        $query = 'select * from users where id = ' . $id . ' limit 1';
        $rs = Database::db()->query($query)->fetch(\PDO::FETCH_ASSOC);
        
        if (false === $rs) {
            throw new UserWasNotLoadedException('id: ' . $id);
        }

        $this->id = $id;
        $this->email = $rs['email'];
        $this->password = $rs['password'];
        $this->username = $rs['username'];
    }

    /**
     * @param string $email
     * @return int
     * @throws UserNotFoundException
     */
    public static function getIdByEmail(string $email) : int
    {
        $query = 'select id from users where email = ? limit 1';
        $statement = Database::db()->prepare($query);
        $statement->execute([$email]);
        $rs = $statement->fetch(\PDO::FETCH_ASSOC);

        if (null === $rs['id']) {
            throw new UserNotFoundException();
        }

        return (int) $rs['id'];
    }

    /**
     * Returns > 0 on success.
     *
     * @param string $email
     * @param string $password
     * @param string $username
     * @return int
     */
    public static function insert(string $email, string $password, string $username) : int
    {
        $pdo = Database::db();
        $query = 'INSERT INTO users (email, password, username) VALUES (?, ?, ?)';
        $statement = $pdo->prepare($query);
        $statement->execute([$email, $password, $username]);
        return (int) $pdo->lastInsertId();
    }

    public static function delete(int $id) : bool
    {
        return Database::db()->prepare('delete from users where id = ? limit 1')->execute([$id]);
    }

    public function login(string $password) : bool
    {
        if ($password == $this->password) {
            Session::getInstance()->setLoggedIn();
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }
}

