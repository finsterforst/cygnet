<?php declare(strict_types=1);

namespace Finsterforst\Cygnet;


class System
{
    private static $instance;
    
    private function __construct() {}
    private function __clone() {}
    
    
    public static function getInstance() : System {
        if (!self::$instance instanceof self) {
            self::$instance = new System();
        }
        return self::$instance;
    }

    /**
     * Anything inside the directory source.
     * @return string
     */
    public static function getBasePath() : string {
        return __DIR__ . '/';
    }

    public function initializeCygnet() : void {
        $this->initializeAutoloader();
    }
    
    public function initializeAutoloader() : void {
        require_once self::getBasePath() . '../vendor/autoload.php';
    }
}