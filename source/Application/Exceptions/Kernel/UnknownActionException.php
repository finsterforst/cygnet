<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Exceptions\Kernel;


class UnknownActionException extends \Exception
{

}