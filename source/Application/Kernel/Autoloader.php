<?php declare(strict_types=1);

namespace Finsterforst\Cygnet\Application\Kernel;

use Finsterforst\Cygnet\Application\System;
use Finsterforst\Cygnet\Application\Exceptions\Kernel\ClassCouldNotBeRequiredException;

class Autoloader
{
    private static $instance;
    
    private function __construct() {}
    private function __clone() {}
    
    public static function getInstance() : Autoloader {
        if (!self::$instance instanceof self) {
            self::$instance = new Autoloader();
            self::$instance->initialize();
        }
        return self::$instance;
    }
    
    private function initialize() : void {
        spl_autoload_register([$this, 'requireClass']);
    }
    
    /**
     * 
     * @param string $class
     * @throws ClassCouldNotBeRequiredException
     */
    public function requireClass(string $class) : void {
        
        // path     : D:\dev\htdocs\Cygnet\Application\..\
        // className: Cygnet\Application\System
        // we want  : 
        //    D:\dev\htdocs\Cygnet\   from path
        //    Application\System      from class
        
        $classPathWithoutProjectNamespace = str_replace('Cygnet\\', '', $class);
        
        $classPath = System::getBasePath() . $classPathWithoutProjectNamespace . '.php';
        
        if (file_exists($classPath)) {
            require_once $classPath;
        } else {
            throw new ClassCouldNotBeRequiredException(
                'Tried to get "' . $class . '" in path "' . $classPath . '"'
            );
        }
    }
}
