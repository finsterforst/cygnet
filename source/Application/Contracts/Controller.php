<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Contracts;


interface Controller
{
    public function preHook() : void;

    public function postHook() : void;
}