<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Controller\Game;


use Finsterforst\Cygnet\Application\Kernel\Router;
use Finsterforst\Cygnet\Application\Kernel\Session;
use Finsterforst\Cygnet\Application\Models\Resources;
use Finsterforst\Cygnet\Application\Models\User;

class Building extends Base
{
    public function indexAction()
    {
        $id = (int) Session::getInstance()->get('user_id');
        $user = new User();
        $user->load($id);

        $building = new \Finsterforst\Cygnet\Application\Models\Building();
        $buildingId = \Finsterforst\Cygnet\Application\Models\Building::getIdByUserAndBuildingId($user, 1);
        $building->load($buildingId);

        $resources = new Resources();
        $resources->load(Resources::getIdByUser($user));


        $this->assign('building_level', $building->getLevel());
        $this->assign('building_cost', ($building->getLevel() + 1) * 1.3);
        $this->assign('money', $resources->getMoney());
    }

    public function buildAction()
    {
        var_dump($_GET);
    }
}