<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Controller\Game;


use Finsterforst\Cygnet\Application\Kernel\Router;
use Finsterforst\Cygnet\Application\Kernel\Session;

class User extends Base
{
    public function logoutAction()
    {
        Session::getInstance()->logout();
        $router = Router::getInstance();
        $router->redirect($router->getDefaultContext(), $router->getDefaultController());
    }
}