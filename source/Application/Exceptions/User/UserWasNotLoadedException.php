<?php declare(strict_types=1);

namespace Finsterforst\Cygnet\Application\Exceptions;

class UserWasNotLoadedException extends \Exception
{
}

