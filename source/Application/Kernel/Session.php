<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Kernel;


use Finsterforst\Cygnet\Application\Exceptions\Session\SessionVariableNotFoundException;
use Finsterforst\Cygnet\Application\Kernel\Session\SessionItem;

class Session
{
    private $id;

    private static $instance;

    private function __construct() {}
    private function __clone() {}

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
            self::$instance->initialize();
        }
        return self::$instance;
    }

    private function initialize() : void
    {
        $status = session_start();
        $this->id = session_id();

        $this->handleExpireItems();
    }


    protected function handleExpireItems() : void
    {
        /**
         * @var string $key
         * @var SessionItem $value
         */
        foreach ($_SESSION as $key => $value) {

            if ($value->isExpire()) {
                $value->subtractRequestsToLife();
            }

            if ($value->getRequestsToLife() < 0) {
                $this->delete($key);
            }
        }
    }

    public function destroySession() : void
    {
        session_destroy();
        $_SESSION = [];
        $this->id = '';
    }

    public function getSessionId() : string {
        return $this->id;
    }

    public function set(string $key, $value = null, $expire = false) : void
    {
        $_SESSION[$key] = new SessionItem($key, $value, $expire);
    }

    /**
     * @param string $key
     * @return mixed
     * @throws SessionVariableNotFoundException
     */
    public function get(string $key)
    {
        if ($this->has($key)) {
            return $_SESSION[$key]->getValue();
        }
        throw new SessionVariableNotFoundException(var_export('Session key not found: "' . $key . '".'));
    }

    public function has(string $key) : bool
    {
        return isset($_SESSION[$key]);
    }

    public function delete(string $key)
    {
        unset($_SESSION[$key]);
    }

    public function setLoggedIn() : void
    {
        $this->set('logged_in', true);
    }

    public function isLoggedIn() : bool
    {
        if ($this->has('logged_in')) {
            return $this->get('logged_in');
        }
        return false;
    }

    public function logout() : void
    {
        $this->set('logged_in', false);
    }
}