<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Controller;


use Finsterforst\Cygnet\Application\Kernel\Router;
use Finsterforst\TemplateEngine\TemplateVariableCollection;

class Controller implements \Finsterforst\Cygnet\Application\Contracts\Controller
{
    public function preHook(): void
    {
        // TODO: Implement preHook() method.
        $this->assign('url', Router::getInstance()->getUrl());
    }

    public function postHook(): void
    {
        // TODO: Implement postHook() method.
    }

    public function assign(string $key, $value)
    {
        TemplateVariableCollection::getInstance()->add($key, $value);
    }
}