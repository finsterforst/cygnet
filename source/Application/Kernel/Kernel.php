<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Kernel;


use Finsterforst\Cygnet\Application\Contracts\Controller;
use Finsterforst\Cygnet\Application\Contracts\Controller as ControllerContract;
use Finsterforst\Cygnet\Application\Exceptions\Kernel\ControllerDisregardsContractException;
use Finsterforst\Cygnet\Application\Exceptions\Kernel\UnknownActionException;
use Finsterforst\Cygnet\System;
use Finsterforst\TemplateEngine\Configuration;
use Finsterforst\TemplateEngine\TemplateEngine;

class Kernel
{
    private static $instance;

    private function __construct() {}
    private function __clone() {}

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function process()
    {
        $controller = $this->executeController();
        $this->displayTemplate($controller);
    }

    protected function executeController() : Controller
    {
        $router = Router::getInstance();
        $controllerNamespacePath = 'Finsterforst\Cygnet\Application\Controller\\' . $router->getContext() . '\\' . $router->getController();

        $controller = new $controllerNamespacePath();

        if (!$controller instanceof ControllerContract) {
            throw new ControllerDisregardsContractException('Controller "' . $controllerNamespacePath . '".');
        }

        $methodName = $router->getAction() . 'Action';

        if (!method_exists($controller, $methodName)) {
            throw new UnknownActionException($controllerNamespacePath . '::' . $methodName);
        }

        $controller->preHook();
        $controller->$methodName();
        $controller->postHook();

        return $controller;
    }

    protected function displayTemplate(Controller $controller) : void
    {
        $router = Router::getInstance();

        $contextPath = System::getBasePath() . 'Application/Views/' . $router->getContext() . '/';
        $templatePath = $contextPath . $router->getController() . '/';

        $templateEngineConfiguration = new Configuration();
        $templateEngineConfiguration->addPath($contextPath);
        $templateEngineConfiguration->addPath($templatePath);

        $templateEngine = TemplateEngine::getInstance();
        $templateEngine->initialise($templateEngineConfiguration);
        $templateEngine->render($router->getAction());
        $templateEngine->display();
    }
}