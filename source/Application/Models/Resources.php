<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Models;


use Finsterforst\Cygnet\Application\Exceptions\User\UserNotFoundException;
use Finsterforst\Cygnet\Application\Exceptions\UserWasNotLoadedException;
use Finsterforst\Cygnet\Application\Kernel\Database;

class Resources
{   /** @var int */
    private $id;
    /** @var User */
    private $user;
    /** @var int */
    private $lastProduced;
    /** @var int */
    private $money;

    /** @var float */
    private $resource_1;

    /**
     *
     * @param int $id
     * @throws UserWasNotLoadedException
     */
    public function load(int $id) : void {
        $query = 'select * from resources where id = ' . $id . ' limit 1';
        $rs = Database::db()->query($query)->fetch(\PDO::FETCH_ASSOC);

        if (false === $rs) {
            throw new UserWasNotLoadedException('id: ' . $id); // TODO
        }

        $this->id = $id;

        $user = new User();
        $user->load((int) $rs['user_id']); // TODO Cache Registry
        $this->user = $user;

        $this->lastProduced = (int) $rs['last_produced'];
        $this->money = (int) $rs['money'];

        $this->resource_1 = (float) $rs['resource_1'];
    }

    public function save()
        {
        $query = 'update resources set last_produced = :last, money = :money where id = :id limit 1';
        $statement = Database::db()->prepare($query);
        $statement->bindValue(':last', $this->lastProduced);
        $statement->bindValue(':money', $this->money);
        $statement->bindValue(':id', $this->id);
        $statement->execute();

        $this->load($this->id);
    }

    /**
     * @param User $user
     * @return int
     * @throws UserNotFoundException
     */
    public static function getIdByUser(User $user) : int
    {
        $query = 'select id from resources where user_id = :user limit 1';
        $statement = Database::db()->prepare($query);
        $statement->bindValue(':user', $user->getId());
        $statement->execute();
        $rs = $statement->fetch(\PDO::FETCH_ASSOC);

        if (null === $rs['id']) {
            throw new UserNotFoundException(); // TODO
        }

        return (int) $rs['id'];
    }

    public function getId() : int
    {
        return $this->id;
    }
    public function getUser() : User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getLastProduced(): int
    {
        return $this->lastProduced;
    }

    public function updateLastProduced()
    {
        $this->lastProduced = time();
        $this->save();

    }

    public function getMoney() : int
    {
        return $this->money;
    }

    /**
     * @param Building[] $buildings
     */
    public function produce(array $buildings)
    {
        foreach ($buildings as $building) {
            $field = 'resource_' . $building->getId();
            $level = $building->getLevel();
            $difference = time() - $this->lastProduced;

            $produced = (float) $level * $difference;
            $this->$field = $produced + $this->$field;

            $sql = 'update resources set ' . $field . ' =  ' . $field . ' + ' . $produced .' where id = :id limit 1';

            $statement = Database::db()->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
    }

    /**
     * @return float
     */
    public function getResource1(): float
    {
        return $this->resource_1;
    }
}