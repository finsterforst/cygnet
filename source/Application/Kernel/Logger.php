<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Kernel;


use Finsterforst\Cygnet\System;

class Logger
{
    private $verbose = false;

    /**
     * Gets filled during initializing
     * @var string
     */
    private $destination;

    private static $instance;

    private function __construct() {}
    private function __clone() {}

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
            self::$instance->destination = System::getBasePath() . 'Logs/';
        }
        return self::$instance;
    }

    public function setVerbose(bool $switch) : void
    {
        $this->verbose = $switch;
    }

    public function log(string $message, string $filename) : void
    {
        $this->write($message, $filename);
    }

    public function info(string $message) : void
    {
        $this->write($message, 'info');
    }

    public function warning(\Exception $exception) : void
    {
        $message = $this->decideMessageVerbosity($exception);
        $this->write($message, 'warning');
    }

    public function error(\Exception $exception) : void
    {
        $message = $this->decideMessageVerbosity($exception);
        $this->write($message, 'error');
    }

    public function fatal(\Exception $exception) : void
    {
        $this->verbose = true;
        $message = $this->decideMessageVerbosity($exception);
        $this->write($message, 'fatal');
        echo 'FATAL ERROR: ' . $exception->getMessage();
        exit;
    }

    protected function decideMessageVerbosity(\Exception $exception) : string
    {
        if ($this->verbose) {
            return print_r($exception, true);
        } else {
            return (new \ReflectionClass($exception))->getShortName() . ': ' . $exception->getMessage();
        }
    }

    protected function write(string $message, string $filename) : void
    {
        $destination = $this->destination . $filename . '.log';

        $microTime = microtime(true);
        $formattedDate = date('d.m.Y H:i:s', (int) $microTime);

        $message = $formattedDate . ' (' . $microTime . '): ' . $message . PHP_EOL;
        error_log($message, 3, $destination);
    }
}