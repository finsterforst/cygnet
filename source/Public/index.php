<?php declare(strict_types=1);


use Finsterforst\Cygnet\Application\Exceptions\Kernel\RouterUnevenParametersException;
use Finsterforst\Cygnet\Application\Exceptions\Kernel\CouldNotLoadTemplateFiles;
use Finsterforst\Cygnet\Application\Exceptions\Kernel\DefaultControllerNotFoundException;


require_once '..' . DIRECTORY_SEPARATOR . 'System.php';

try {
    Finsterforst\Cygnet\System::getInstance()->initializeCygnet();
    Finsterforst\Cygnet\Application\Kernel\Kernel::getInstance()->process();
} catch (DefaultControllerNotFoundException | CouldNotLoadTemplateFiles $ex) {
    Finsterforst\Cygnet\Application\Kernel\Logger::getInstance()->fatal($ex);
} catch (RouterUnevenParametersException | Exception $ex) {
    Finsterforst\Cygnet\Application\Kernel\Logger::getInstance()->error($ex);
    $router = Finsterforst\Cygnet\Application\Kernel\Router::getInstance();
    $router->redirect($router->getDefaultContext(), $router->getDefaultController());
}
