<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Controller\Home;


use Finsterforst\Cygnet\Application\Contracts\Controller as ControllerContract;
use Finsterforst\Cygnet\Application\Controller\Controller;
use Finsterforst\Cygnet\Application\Exceptions\User\UserNotFoundException;
use Finsterforst\Cygnet\Application\Exceptions\User\UserWrongPassword;
use Finsterforst\Cygnet\Application\Kernel\Logger;
use Finsterforst\Cygnet\Application\Kernel\Router;
use Finsterforst\Cygnet\Application\Kernel\Session;
use Finsterforst\Cygnet\Application\Kernel\TemplateEngine;
use Finsterforst\Cygnet\Application\Models\TemplateVariableCollection;
use Finsterforst\Cygnet\Application\Models\User;

class Start extends Controller
{
    public function indexAction() : void
    {
        if (Session::getInstance()->has('login_error')) {
            $this->assign('login_error', Session::getInstance()->get('login_error'));
        }
    }

    public function loginAction() : void
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        try {
            $user = new User();
            $user->load(User::getIdByEmail($username));
            if ($user->login($password)) {
                Session::getInstance()->set('user_id', $user->getId());
                Router::getInstance()->redirect('Game', 'Start');
            }
        } catch (UserNotFoundException $ex) {
            Session::getInstance()->set('login_error', 'user not found', true);
            $router = Router::getInstance();
            $router->redirect($router->getDefaultContext(), $router->getDefaultController());
        }
    }

}