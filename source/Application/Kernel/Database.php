<?php declare(strict_types=1);

namespace Finsterforst\Cygnet\Application\Kernel;

class Database
{
	private static $instance;

	/** @var \PDO */
	private $db;
	
	private function __construct() {}
	private function __clone() {}
	
	public static function getInstance()
	{
		if (!self::$instance instanceof Database) {
			self::$instance = new Database();
			self::$instance->init();
		}
		return self::$instance;
	}
	
	private function init()
	{
		$this->db = new \PDO('mysql:host=localhost;dbname=cygnet', 'root', '');
	}
	
	public static function db()
	{
		return self::getInstance()->db;
	}
}
