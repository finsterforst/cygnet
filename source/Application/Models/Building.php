<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Models;


use Finsterforst\Cygnet\Application\Exceptions\User\UserNotFoundException;
use Finsterforst\Cygnet\Application\Exceptions\UserWasNotLoadedException;
use Finsterforst\Cygnet\Application\Kernel\Database;

class Building extends BaseModel
{
    private $id;

    private $user;

    private $buildingId;

    private $level;



    /**
     *
     * @param int $id
     * @throws UserWasNotLoadedException
     */
    public function load(int $id) : void {
        $query = 'select * from buildings where id = ' . $id . ' limit 1';
        $rs = Database::db()->query($query)->fetch(\PDO::FETCH_ASSOC);

        if (false === $rs) {
            throw new UserWasNotLoadedException('id: ' . $id); // TODO
        }

        $this->id = $id;

        $user = new User();
        $user->load((int) $rs['user_id']); // TODO Cache Registry
        $this->user = $user;

        $this->buildingId = (int) $rs['building_id'];
        $this->level = (int) $rs['level'];
    }

    public function save()
    {
        $query = 'update buildings set level = :level where id = :id limit 1';
        $statement = Database::db()->prepare($query);
        $statement->bindValue(':level', $this->level);
        $statement->bindValue(':id', $this->id);
        $statement->execute();

        $this->load($this->id);
    }

    /**
     * @param string $email
     * @return int
     * @throws UserNotFoundException
     */
    public static function getIdByUserAndBuildingId(User $user, int $buildingId) : int
    {
        $query = 'select id from buildings where user_id = :user and building_id = :building limit 1';
        $statement = Database::db()->prepare($query);
        $statement->bindValue(':user', $user->getId());
        $statement->bindValue('building', $buildingId);
        $statement->execute();
        $rs = $statement->fetch(\PDO::FETCH_ASSOC);

        if (null === $rs['id']) {
            throw new UserNotFoundException(); // TODO
        }

        return (int) $rs['id'];
    }

    /**
     * Returns > 0 on success.
     *
     * @param string $email
     * @param string $password
     * @param string $username
     * @return int
     */
    public static function insert(string $email, string $password, string $username) : int
    {
        // INSERT INTO `buildings` (`id`, `user_id`, `building_id`, `level`) VALUES (NULL, '1', '1', '0');
        $pdo = Database::db();
        $query = 'INSERT INTO users (email, password, username) VALUES (?, ?, ?)';
        $statement = $pdo->prepare($query);
        $statement->execute([$email, $password, $username]);
        return (int) $pdo->lastInsertId();
    }

    public static function delete(int $id) : bool
    {
        return Database::db()->prepare('delete from users where id = ? limit 1')->execute([$id]);
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getUser() : User
    {
        return $this->user;
    }

    public function getBuildingId() : int
    {
        return $this->buildingId;
    }

    public function getLevel() : int
    {
        return $this->level;
    }
}