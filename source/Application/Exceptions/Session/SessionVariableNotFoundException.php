<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Exceptions\Session;


class SessionVariableNotFoundException extends \Exception
{

}