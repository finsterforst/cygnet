<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Kernel;


use Finsterforst\Cygnet\Application\Exceptions\Kernel\DefaultControllerNotFoundException;
use Finsterforst\Cygnet\Application\Exceptions\Kernel\RouterUnevenParametersException;
use Finsterforst\Cygnet\Application\Exceptions\Kernel\RouterUnknownParameter;
use Finsterforst\Cygnet\System;

class Router
{
    private $url;

    private $context = '';
    private $controller = '';
    private $action = '';
    /**
     * key => value
     * @var array
     */
    private $parameters = [];

    private $allowedContexts = [
        'Home',
        'Game'
    ];

    private static $instance;

    private function __construct() {}
    private function __clone() {}

    /**
     * @return Router
     * @throws DefaultControllerNotFoundException
     * @throws RouterUnevenParametersException
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
            self::$instance->initialize();
        }
        return self::$instance;
    }

    public function redirect($context, $controller, $action = 'index')
    {
        Logger::getInstance()->log($_SERVER['REQUEST_URI'] . ' -> ' . '/' . $context . '/' . $controller . '/' . $action, 'router');
        header('Location: /' . $context . '/' . $controller . '/' . $action);
        exit;
    }

    /**
     * @throws DefaultControllerNotFoundException
     * @throws RouterUnevenParametersException
     */
    private function initialize() : void
    {
        // TODO: own function
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
        $this->url = $protocol . $_SERVER['HTTP_HOST'] . '/';

        $uri = $_SERVER['REQUEST_URI'];

        // Clean up the URI
        $uri = $this->splitUriStringToArray($uri);
        $uri = $this->removeEmptyUriElements($uri);

        if (count($uri) % 2 === 0) {
            throw new RouterUnevenParametersException('Route: ' . json_encode($uri));
        }

        // Do the URI!
        $this->checkForContext($uri);
        $this->checkForController($uri);
        $this->checkForAction($uri);
        $this->checkForParameters($uri);

        // If we are at this point, then the URI looks at least like this: localhost/Home/Start/index

        /*
         * In case the controller is not found there will be a redirect to the default controller.
         * But if the controller is not found and the controller was the default controller, then we
         * have some serious problems and we do a shutdown here (preventing an endless loop).
         */
        if (!$this->doesControllerExist()) {
            if ($this->context === $this->getDefaultContext() && $this->controller === $this->getDefaultController()) {
                throw new DefaultControllerNotFoundException(
                    'Default context or controller is missing!
                    Context: ' . $this->context . '; controller: ' . $this->controller
                );
            }
            $this->redirect($this->getDefaultContext(), $this->getDefaultController());
        }
    }

    /**
     * @param string $uri
     * @return string[]
     */
    protected function splitUriStringToArray(string $uri) : array
    {
        $uri = explode('/', $uri);

        // Removes the first entry (which is the domain name) and starts right after the first slash.
        array_shift($uri);

        // only two key value params
        if (count($uri) > 7) {
            $this->redirect($this->getDefaultContext(), $this->getDefaultController());
        }
        return $uri;
    }

    /**
     * @param string[] $uri
     * @return string[]
     */
    protected function removeEmptyUriElements(array $uri) : array
    {
        return array_filter($uri, function($element) {
            return !empty($element);
        });
    }

    /**
     * 0: context
     * 1: controller
     * 2: action
     * 3 + 4: first parameter key value wise.
     * 5 + 6: ..
     * @param array $uri
     * @throws RouterUnevenParametersException
     */
    protected function checkForParameters(array $uri) : void
    {
        unset($uri[0], $uri[1], $uri[2]);
        $uri = array_values($uri);
        $itemQuantity = count($uri);

        if ($itemQuantity % 2 !== 0) {
            throw new RouterUnevenParametersException('Route: ' . json_encode($uri));
        }

        for ($i = 0; $i < $itemQuantity; $i = $i + 2) {
            $this->parameters[$uri[$i]] = $uri[$i + 1];
        }
    }

    /**
     * Checking for: localhost/Home
     *
     * @param array $uri
     */
    protected function checkForContext(array $uri) : void
    {
        if (!array_key_exists(0, $uri) || empty($uri[0])) {
            $this->redirect($this->getDefaultContext(), $this->getDefaultController());
        }

        // key 0 does exist, we can work with the uri.
        $this->context = $this->sanitizeControllerParameters($uri[0]);

        // Controller is missing
        if (empty($this->context) || !in_array($this->context, $this->allowedContexts)) {
            $this->redirect($this->getDefaultContext(), $this->getDefaultController());
        }
    }

    /**
     * Checking for: localhost/Home/Start
     *
     * @param array $uri
     */
    protected function checkForController(array $uri) : void
    {
        if (!array_key_exists(1, $uri)) {
            $this->redirect($this->context, $this->getDefaultController());
        }

        // key 1 does exist, we can work with the uri.
        $this->controller = $this->sanitizeControllerParameters($uri[1]);

        // Controller is missing
        if (empty($this->controller)) {
            $this->redirect($this->context, $this->getDefaultController());
        }

        $this->controller = ucfirst($this->controller);
    }

    /**
     * Checking for: localhost/Home/Start/index
     *
     * @param array $uri
     */
    protected function checkForAction(array $uri) : void
    {
        if (array_key_exists(2, $uri)) {
            $this->action = $this->sanitizeControllerParameters($uri[2]);
        }

        if (empty($this->action)) {
            $this->action = 'index';
        }
    }

    protected function doesControllerExist() : bool
    {
        $pathToController = System::getBasePath() . 'Application/Controller/' . $this->context . '/' . $this->controller . '.php';
        return file_exists($pathToController);
    }

    protected function sanitizeControllerParameters($value) : string
    {
        $value = preg_replace('/\W/', '', $value); // a-Z 0-9
        $value = preg_replace('/[0-9]/', '', $value); // a-Z
        return $value;
    }

    public function getDefaultContext() : string
    {
        return $this->allowedContexts[0];
    }

    public function getDefaultController() : string
    {
        return 'Start';
    }

    public function getContext() : string
    {
        return $this->context;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getUrl() : string
    {
        return $this->url;
    }

    public function hasParameter(string $key) : bool
    {
        return key_exists($key, $this->parameters);
    }

    /**
     * @param string $key
     * @return mixed
     * @throws RouterUnknownParameter
     */
    public function getParameter(string $key)
    {
        if (!$this->hasParameter($key)) {
            throw new RouterUnknownParameter($key);
        }
        return $this->parameters[$key];
    }
}