<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Kernel\Session;


class SessionItem
{
    private $key;

    private $value;

    private $expire;

    private $requestsToLife;

    public function __construct(string $key, $value, bool $expire = false, int $requestsToLife = 1)
    {
        $this->key = $key;
        $this->value = $value;
        $this->expire = $expire;
        $this->requestsToLife = $requestsToLife;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isExpire(): bool
    {
        return $this->expire;
    }

    /**
     * @return int
     */
    public function getRequestsToLife(): int
    {
        return $this->requestsToLife;
    }

    public function subtractRequestsToLife(): void
    {
        --$this->requestsToLife;
    }
}
