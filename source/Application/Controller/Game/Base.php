<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Controller\Game;


use Finsterforst\Cygnet\Application\Controller\Controller;
use Finsterforst\Cygnet\Application\Kernel\Router;
use Finsterforst\Cygnet\Application\Kernel\Session;
use Finsterforst\Cygnet\Application\Models\Building as BuildingModel;
use Finsterforst\Cygnet\Application\Models\Resources;
use Finsterforst\Cygnet\Application\Models\User;

class Base extends Controller
{
    /** @var User */
    protected $user;

    /** @var Resources */
    protected $resources;

    public function preHook(): void
    {
        parent::preHook();

        if (!Session::getInstance()->isLoggedIn()) {
            $router = Router::getInstance();
            $router->redirect($router->getDefaultContext(), $router->getDefaultController());
        }

        $this->user = new User();
        $this->user->load((int) Session::getInstance()->get('user_id'));

        $this->resources = new Resources();
        $this->resources->load(Resources::getIdByUser($this->user));

        $buildingResource1 = new BuildingModel();
        $buildingResource1->load(BuildingModel::getIdByUserAndBuildingId($this->user, 1));
        $this->resources->produce([$buildingResource1]);

        $this->resources->updateLastProduced();

        $this->assign('money', $this->resources->getMoney());
        $this->assign('resource1', $this->resources->getResource1());
    }
}