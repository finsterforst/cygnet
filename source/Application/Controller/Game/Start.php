<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Controller\Game;


use Finsterforst\Cygnet\Application\Contracts\Controller;
use Finsterforst\Cygnet\Application\Kernel\Session;
use Finsterforst\Cygnet\Application\Models\TemplateVariableCollection;
use Finsterforst\Cygnet\Application\Models\User;

class Start extends Base
{
    public function indexAction()
    {
        $id = (int) Session::getInstance()->get('user_id');
        $user = new User();
        $user->load($id);

        $this->assign('user_name', $user->getUsername());
    }
}