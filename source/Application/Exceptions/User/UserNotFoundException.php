<?php declare(strict_types=1);


namespace Finsterforst\Cygnet\Application\Exceptions\User;


class UserNotFoundException extends \Exception
{

}